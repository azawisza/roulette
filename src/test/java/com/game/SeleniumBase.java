package com.game;

import com.game.Application;
import com.game.MockConfiguration;
import org.fluentlenium.adapter.FluentTest;
import org.fluentlenium.adapter.util.SharedDriver;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.concurrent.TimeUnit;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {WebMvcConfiguration.class, MockConfiguration.class})
@IntegrationTest({
        "mongodb.database:roulette",
        "mongodb.username:username",
        "mongodb.password:",
        "mongodb.host:localhost",
        "websockets.allowedorigins[0]:*",
        "websockets.allowedorigins[1]:http://test.com",
        "websockets.allowedorigins[2]:*",
        "websockets.allowedorigins[3]:*",
        "websockets.serverUrl:http://localhost",
        "websockets.serverSuffix: game",
        "game.initialGameDelay:1",
        "server.port:8070",
        "spring.profile:dev",
        "game.roundTime:2",
        "mongodb.port:11"})
@WebAppConfiguration
@SharedDriver(type = SharedDriver.SharedType.PER_CLASS)
@Ignore
public class SeleniumBase extends FluentTest{

    @Override
    public WebDriver getDefaultDriver() {
        ChromeOptions options = new ChromeOptions();
        WebDriver webDriver = new ChromeDriver(options);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        return webDriver;
    }

}
