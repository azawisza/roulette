package com.game;

import com.github.fakemongo.Fongo;
import com.google.common.eventbus.EventBus;
import com.mongodb.DB;
import com.game.roulette.domain.game.Game;
import com.game.roulette.domain.game.GamesRepository;
import com.game.roulette.domain.game.WinningRouletteNumberSupplier;
import org.jongo.Jongo;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.net.UnknownHostException;
import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class MockConfiguration {

    @Value("${mongodb.database}")
    public String database;

    @Bean
    public Jongo createJongo() throws UnknownHostException {
        DB db = new Fongo("Test").getDB("Database");
        Jongo jongo = new Jongo(db);
        return jongo;
    }

    @Bean
    @Primary
    public WinningRouletteNumberSupplier createWinningNumberSupplier() {
        return Mockito.mock(WinningRouletteNumberSupplier.class);
    }

    @Bean
    @Primary
    public EventBus createEventBus() {
        return Mockito.mock(EventBus.class);
    }

    @Bean
    @Primary
    public GamesRepository createGamesRepository() {
        GamesRepository mock = Mockito.mock(GamesRepository.class);
        when(mock.findOne(any(Date.class))).thenReturn(new Game().with_id("abc").withWinningNumber(12));
        return mock;

    }

}

