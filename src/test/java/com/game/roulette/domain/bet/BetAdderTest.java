package com.game.roulette.domain.bet;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Mockito.*;

public class BetAdderTest {

    @Test
    public void shouldAddNewBet() {
        //given
        BetsRepository repository = Mockito.mock(BetsRepository.class);
        ArrayList<Bet> anEmptyList = new ArrayList<Bet>();
        when(repository.findBet(any(Bet.class))).thenReturn(anEmptyList);
        BetAdder adder = new BetAdder(repository);
        //when
        Bet test = new Bet("test", 12);
        adder.addIfAbsent(test);
        //then
        verify(repository, atLeastOnce()).insert(test);
    }


    @Test
    public void shouldNotAddBetWhenPresent() {
        //given
        BetsRepository repository = Mockito.mock(BetsRepository.class);
        when(repository.findBet(any(Bet.class))).thenReturn(newArrayList(new Bet()));
        BetAdder adder = new BetAdder(repository);
        //when
        Bet test = new Bet("test", 12);
        adder.addIfAbsent(test);
        //then
        verify(repository, never()).insert(test);
    }

}