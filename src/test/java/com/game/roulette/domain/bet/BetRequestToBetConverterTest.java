package com.game.roulette.domain.bet;

import org.junit.Test;

import static com.game.roulette.TestHelper.bet;
import static com.game.roulette.domain.bet.BetParity.ODD;
import static org.fest.assertions.Assertions.assertThat;

public class BetRequestToBetConverterTest {

    private BetRequestToBetConverter converter = new BetRequestToBetConverter();

    @Test
    public void shouldConvertToParityBet() {
        //given
        Bet expected = new Bet("user", ODD).withStake("12");
        //when
        Bet convertedBet = converter.convert(bet("ODD"));
        //then
        assertThat(convertedBet).isEqualTo(expected);
    }

    @Test
    public void shouldConvertToNumericBet() {
        //given
        Bet expected = new Bet("user", 12).withStake("12");
        //expected
        Bet convertedBet = converter.convert(bet("12"));
        //then
        assertThat(convertedBet).isEqualTo(expected);
    }

}