package com.game.roulette.service;

import com.game.roulette.domain.game.Game;
import com.game.roulette.domain.game.GamesRepository;
import com.game.roulette.domain.game.WinningRouletteNumberSupplier;
import com.game.roulette.domain.game.events.CurrentGameChange;
import com.game.roulette.domain.game.events.PublishLastGameResults;
import com.game.roulette.service.model.RouletteStatus;
import com.game.roulette.service.model.UserBetRequest;
import com.google.common.eventbus.EventBus;
import com.game.IntegrationTestBase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RouletteIntegrationTest extends IntegrationTestBase {

    @Autowired
    public RouletteService service;

    @Autowired
    public WinningRouletteNumberSupplier winningNumberSupplier;

    @Autowired
    public EventBus eventBus;

    @Autowired
    private GamesRepository gamesRepository;

    @Before
    public void setUp() {
        when(gamesRepository.findOne(any(Date.class))).thenReturn(new Game().with_id("abc").withWinningNumber(12));
    }


    @Test
    public void shouldNotAcceptTooLongUserName() {
        //given
        when(winningNumberSupplier.getTheWinningNumber()).thenReturn(12);
        //when
        RouletteStatus result = service.placeBet(new UserBetRequest().withUsername("Alice123abcdefghijllmnopqrst").withBet("1211").withStake("11"));
        //then
        assertThat(result).isEqualTo(RouletteStatus.BET_INVALID);
        assertThat("Bet is invalid ").isEqualTo(RouletteStatus.BET_INVALID.getMessage());
    }

    @Test
    public void shouldNotAcceptInvalidBet() {
        //given
        when(winningNumberSupplier.getTheWinningNumber()).thenReturn(12);
        //when
        RouletteStatus result = service.placeBet(new UserBetRequest().withUsername("Alice").withBet("1211").withStake("11"));
        //then
        assertThat(result).isEqualTo(RouletteStatus.BET_INVALID);
        assertThat(RouletteStatus.BET_INVALID.getMessage()).isEqualTo("Bet is invalid ");
    }

    @Test
    public void shouldNotAcceptInvalidStake() {
        //given
        when(winningNumberSupplier.getTheWinningNumber()).thenReturn(12);
        //when
        RouletteStatus result = service.placeBet(new UserBetRequest().withUsername("Alice").withBet("12").withStake("-"));
        //then
        assertThat(result).isEqualTo(RouletteStatus.BET_INVALID);
        assertThat(RouletteStatus.BET_INVALID.getMessage()).isEqualTo("Stake is invalid ");
    }


    @Test
    public void shouldPlayEndToEnd() {
        //given
        //when
        //then
        verify(eventBus, timeout(8000).times(2)).post(any(CurrentGameChange.class));
        verify(eventBus, timeout(8000).times(2)).post(any(PublishLastGameResults.class));
    }


}
