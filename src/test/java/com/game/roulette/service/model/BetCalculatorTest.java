package com.game.roulette.service.model;

import com.game.roulette.domain.bet.Bet;
import com.game.roulette.domain.bet.BetParity;
import com.game.roulette.domain.bet.BetReport;
import com.game.roulette.domain.bet.Outcome;
import com.game.roulette.domain.game.BetCalculator;
import org.junit.Test;

import java.math.BigInteger;

import static org.fest.assertions.Assertions.assertThat;

public class BetCalculatorTest {

    @Test
    public void shouldCalculateLosingBet() {
        //given
        BetCalculator calculator = new BetCalculator(10);
        //when
        PlayerBetResult result = calculator.calculate(new BetReport(new Bet("Smith", 12), new BigInteger("123")));
        //then
        assertThat(result).isEqualTo(new PlayerBetResult()
                .withBet("12").withOutcome(Outcome.LOSE).withUser("Smith").withWinnings("0"));
    }

    @Test
    public void shouldCalculateWinningBet() {
        //given
        BetCalculator calculator = new BetCalculator(12);
        //when
        PlayerBetResult result = calculator.calculate(new BetReport(new Bet("Smith", 12), new BigInteger("123")));
        //then
        assertThat(result).isEqualTo(new PlayerBetResult()
                .withBet("12").withOutcome(Outcome.WIN).withUser("Smith").withWinnings("4428"));
    }

    @Test
    public void shouldCalculateWinningOddBet() {
        //given
        BetCalculator calculator = new BetCalculator(12);
        //when
        PlayerBetResult result = calculator.calculate(new BetReport(new Bet("Smith", BetParity.ODD), new BigInteger("123")));
        //then
        assertThat(result).isEqualTo(new PlayerBetResult()
                .withBet("ODD").withOutcome(Outcome.LOSE).withUser("Smith").withWinnings("0"));
    }

    @Test
    public void shouldCalculateLoosingEvenBet() {
        //given
        BetCalculator calculator = new BetCalculator(12);
        //when
        PlayerBetResult result = calculator.calculate(new BetReport(new Bet("Smith", BetParity.EVEN), new BigInteger("123")));
        //then
        assertThat(result).isEqualTo(new PlayerBetResult()
                .withBet("EVEN").withOutcome(Outcome.WIN).withUser("Smith").withWinnings("246"));
    }

}