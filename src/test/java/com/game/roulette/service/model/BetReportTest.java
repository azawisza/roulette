package com.game.roulette.service.model;

import com.game.roulette.domain.bet.Bet;
import com.game.roulette.domain.bet.BetReport;
import com.game.roulette.domain.bet.Outcome;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.math.BigInteger;


public class BetReportTest {

    private BetReport report = new BetReport(new Bet("12", 2), new BigInteger("321"));

    @Test
    public void shouldShowNumeric() {
        //given
        //when
        boolean numeric = report.isNumeric();
        //then
        Assertions.assertThat(numeric).isTrue();
    }

    @Test
    public void shouldShowWinning() {
        //given
        //when
        boolean wining = report.isWinning(2);
        //then
        Assertions.assertThat(wining).isTrue();
    }

    @Test
    public void shouldShowOutcome() {
        //given
        //when
        Outcome outcome = report.getOutcome(2);
        //then
        Assertions.assertThat(outcome).isEqualTo(Outcome.WIN);
    }


}