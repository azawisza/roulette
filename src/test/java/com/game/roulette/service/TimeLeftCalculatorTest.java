package com.game.roulette.service;

import com.game.roulette.commons.RouletteConfiguration;
import com.game.roulette.domain.game.Game;
import com.google.common.base.Optional;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;

public class TimeLeftCalculatorTest {

    private RouletteConfiguration conf;
    private CurrentTimeProvider timeProvider;

    @Before
    public void setUp() {
        conf = Mockito.mock(RouletteConfiguration.class);
        timeProvider = Mockito.mock(CurrentTimeProvider.class);
        Mockito.when(timeProvider.getCurrentTime()).thenReturn(3900L);
    }

    @Test
    public void shouldCalculateTime() {
        //given
        TimeLeftCalculator calculator = new TimeLeftCalculator(conf, timeProvider);
        Mockito.when(conf.getRoundTime()).thenReturn(15);
        //when
        String actual = calculator.calculateTimeToCurrentGameEnd(Optional.of(new Game(new Date(3600))));
        //then
        Assertions.assertThat(actual).isEqualTo("00:14");
    }

    @Test
    public void shouldCalculateTimeWithMinutes() {
        //given
        TimeLeftCalculator calculator = new TimeLeftCalculator(conf, timeProvider);
        Mockito.when(conf.getRoundTime()).thenReturn(180);
        //when
        String actual = calculator.calculateTimeToCurrentGameEnd(Optional.of(new Game(new Date(3600))));
        //then
        Assertions.assertThat(actual).isEqualTo("02:59");
    }
}
