package com.game.roulette;

import com.game.roulette.service.model.UserBetRequest;

public class TestHelper {

    public static UserBetRequest bet(String stake) {
        return new UserBetRequest().withBet(stake).withStake("12").withUsername("user");
    }
}
