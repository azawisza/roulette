package com.game.roulette.user.service;

import com.game.user.domain.User;
import com.game.user.domain.UserConverter;
import com.game.user.domain.UsersRepository;
import com.game.user.service.UserService;
import com.game.user.service.model.RegistrationResultStatus;
import com.game.user.service.model.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;

public class UserServiceImplTest {

    private UserService service;
    private UsersRepository userRepository;
    private User user = new User().withPassword("pass").withUsername("name");
    private UserDto userDto = new UserDto().withPassword("pass").withUsername("name");


    @Before
    public void setUp() {
        userRepository = Mockito.mock(UsersRepository.class);
        service = new UserService(userRepository, new UserConverter());
    }


    @Test
    public void shouldAddUserIfAbsent() {
        //given
        Mockito.when(userRepository.findUser(any(String.class), any(String.class))).thenReturn(null);
        //when
        RegistrationResultStatus userStatus = service.addUserIfAbsent(userDto);
        //then
        assertThat(userStatus).isEqualTo(RegistrationResultStatus.ADDED);
    }

    @Test
    public void shouldNotAddUserIfPresent() {
        //given
        Mockito.when(userRepository.findUser(any(String.class))).thenReturn(new User());
        //when
        RegistrationResultStatus userStatus = service.addUserIfAbsent(userDto);
        //then
        assertThat(userStatus).isEqualTo(RegistrationResultStatus.EXISTS_ALREADY);
    }

    @Test
    public void shouldShowThatUserIsAuthenticated() {
        //given
        Mockito.when(userRepository.findUser(any(String.class), any(String.class))).thenReturn(new User());
        //when
        boolean result = service.authenticate(userDto);
        //then
        assertThat(result).isTrue();
    }


}