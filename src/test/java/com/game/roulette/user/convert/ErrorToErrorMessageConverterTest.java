package com.game.roulette.user.convert;

import com.google.common.collect.Lists;
import com.game.commons.ErrorToErrorMessageConverter;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.List;

import static org.mockito.Mockito.when;

public class ErrorToErrorMessageConverterTest {

    private ErrorToErrorMessageConverter converter = new ErrorToErrorMessageConverter();

    @Test
    public void shouldConvertErrors() {
        //given
        Errors errors = Mockito.mock(Errors.class);
        List<ObjectError> objectErrors = Lists.newArrayList();
        objectErrors.add(new ObjectError("object", "This is error"));
        objectErrors.add(new ObjectError("object", "This is error2"));
        when(errors.getAllErrors()).thenReturn(objectErrors);
        //when
        List<String> result = converter.convert(errors);
        //then
        Assertions.assertThat(result).isEqualTo(expectedResult());

    }

    private List<String> expectedResult() {
        List<String> expected = Lists.newArrayList();
        expected.add("This is error");
        expected.add("This is error2");
        return expected;
    }
}