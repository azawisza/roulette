package com.game.roulette.user.convert;

import com.game.user.domain.User;
import com.game.user.domain.UserConverter;
import com.game.user.service.model.UserDto;
import org.fest.assertions.Assertions;
import org.junit.Test;

public class UserConverterTest {

    private UserConverter converter = new UserConverter();

    @Test
    public void shouldConvertDomainUserToUser() {
        //given
        //when
        UserDto user = converter.convert(new User().withPassword("pass").withUsername("name"));
        //then
        Assertions.assertThat(user).isEqualTo(new UserDto("name", "pass"));
    }

    @Test
    public void shouldConvertUserToDomainUser() {
        //given
        //when
        User user = converter.convert(new UserDto("name", "pass"));
        //then
        Assertions.assertThat(user).isEqualTo(new User().withPassword("pass").withUsername("name"));
    }
}