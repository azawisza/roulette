package com.game.roulette.user.selenium;

import com.game.SeleniumBase;
import org.fluentlenium.core.annotation.Page;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by cx2 on 2015-07-02.
 */
public class RegisterFormTest extends SeleniumBase {

    @Value("${server.port}")
    public String applicationPort;

    @Page
    public Helper helper;

    @BeforeClass
    public static void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
    }

    @Test
    @Ignore
    public void shouldNotAllowAddUserWhenEmptyForm() throws InterruptedException {
        //given
        //when
        goTo("http://localhost:" + applicationPort + "/register");
        //then
        helper.await().atMost(10, TimeUnit.SECONDS).until("input[type=\"submit\"]").isPresent();
    }
}

