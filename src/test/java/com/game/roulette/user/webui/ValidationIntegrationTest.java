package com.game.roulette.user.webui;

import com.game.IntegrationTestBase;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class ValidationIntegrationTest extends IntegrationTestBase {

    @Test
    public void shouldPassToLogin() throws Exception {
        this.mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("webui/login"));
    }

    @Test
    public void shouldShowInvalidBecauseOfEmptyCredentials() throws Exception {
        this.mockMvc.perform(post("/loginCheck").param("username", "").param("password", ""))
                .andExpect(model().hasErrors()).andExpect(model().errorCount(4));
    }


}