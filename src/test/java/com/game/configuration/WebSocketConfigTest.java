package com.game.configuration;

import com.game.IntegrationTestBase;
import com.google.common.collect.Lists;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;


public class WebSocketConfigTest extends IntegrationTestBase {

    @Autowired
    public WebSocketConfig config;

    @Test
    public void shouldWireWebsocketConfigurationForOrigin() {
        //given
        List<String> expectedSetOfOrigins = newArrayList("*", "http://test.com", "*", "*");
        //when
        List<String> allowedorigins = config.allowedOrigins.getAllowedorigins();
        //then
        Assertions.assertThat(allowedorigins).isEqualTo(expectedSetOfOrigins);
    }
}