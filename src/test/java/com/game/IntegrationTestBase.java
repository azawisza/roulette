package com.game;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static java.lang.System.setProperty;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {WebMvcConfiguration.class, MockConfiguration.class})
@WebAppConfiguration
@IntegrationTest({
        "mongodb.database:roulette",
        "mongodb.username:username",
        "mongodb.password:",
        "mongodb.host:localhost",
        "websockets.allowedorigins[0]:*",
        "websockets.allowedorigins[1]:http://test.com",
        "websockets.allowedorigins[2]:*",
        "websockets.allowedorigins[3]:*",
        "websockets.serverUrl:http://localhost",
        "websockets.serverSuffix: game",
        "game.initialGameDelay:1",
        "server.port:8070",
        "spring.profile:dev",
        "game.roundTime:2",
        "mongodb.port:11"})
public abstract class IntegrationTestBase  {

    @Autowired
    protected WebApplicationContext context;
    protected MockMvc mockMvc;

    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";
    public static final String SPRING_PROFILE = "spring.active.profile";

    @BeforeClass
    public static void setUpBeforeClass() {
        setProperty(SPRING_PROFILE, SPRING_PROFILE_DEVELOPMENT);
    }

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

}
