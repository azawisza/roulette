<html>
<head>
    <title>Error </title>
</head>
<body>
<a href="login">Go login page</a> <br>
<br>
<br>

<h3>Application error</h3>

<#if error??>
<b>Error Code:</b>${error.code}<br>
<b>Message:</b>   ${error.message}<br>
<b>Details:</b>
<pre style="background-color: #c4e3f3">${error.details}</pre>
</#if>

</body>
</html>