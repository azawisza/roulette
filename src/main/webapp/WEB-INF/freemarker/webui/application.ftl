<html>
<head>
    <title>Hello WebSocket</title>
    <script src="/resources/lib/jquery/jquery-1.8.2.min.js"></script>
    <script src="/resources/lib/jquery/jquery.min.js"></script>
    <script src="/resources/lib/messaging/sockjs-0.3.4.js"></script>
    <script src="/resources/lib/messaging/stomp.js"></script>
    <script type="text/javascript">
        var stompClient = null;
        var serverUrl = "http://roulette-azawisza.rhcloud.com:8000/roulette/game"
        <#if serverurl ??>
        serverUrl = "${serverurl}";
        </#if>

        function connect() {
            disconnect();
            var socket = new SockJS(serverUrl);
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function (frame) {

                console.log('Connected: ' + frame);
                stompClient.subscribe('/topic/publishResults', function (greeting) {
                    showGreeting(JSON.parse(greeting.body));
                });
                stompClient.subscribe('/topic/publishTime', function (greeting) {
                    showTime(JSON.parse(greeting.body));
                });
            });
        }

        function disconnect() {
            if (stompClient != null) {
                stompClient.disconnect();
            }
            console.log("Disconnected");
        }

        function sendName() {
            var name = document.getElementById('name').value;
            stompClient.send("/app/hello", {}, JSON.stringify({'name': name}));
        }

        function showGreeting(message) {
            var rezults = message.result;
            var message = "Winning number: " + message.winningNumber + "<br>";
            for (var i = 0; i < rezults.length; i++) {
                var rez = rezults[i];
                message += " " + rez.player + " " + rez.bet + " outcome: " + rez.outcome + " winnings: " + rez.winnings + "<br>";
            }
            $("#results").html(message);
        }

        function showTime(message) {
            $("#time").text("Round ends in " + message.update);
        }

    </script>
</head>
<body onload="connect()">
<a href="login">Logout </a> <br>
<br>
<br>

<h3>Roulette</h3>
<#if username ??>
    <h4>Welcome ${username} !</h4>
</#if>


<table>

    <tr>
        <td>
            <form action="placeBet" method="post">

                <input type="hidden" name="username" value="${username}"/><br><br>
                Bet (Number from 1 to 36, EVEN,ODD):<br>
                <input type="text" name="bet"/><br>
                Stake:<br>
                <input type="text" name="stake" value="12"/><br><br>

                <input type="submit" value="Place Bet"/>

            </form>
        </td>
    </tr>
</table>

<br><br>
<#if message??>
<h3>${message} </h3>
</#if>

<p id="time"></p>


<p id="results"></p>

</div>


</body>
</html>

