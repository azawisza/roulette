<html>
<head>
    <title>Login</title>
</head>
<body>

<form action="loginCheck" method="post">
    Login:<br>
    <input type="text" name="username"/><br>
    Password:<br>
    <input type="text" name="password"/>
    <br><br>
    <input type="submit" value="Login"/>
</form>

<h3>${message}</h3>

<#if errors??>
    <#list errors as error>
    <a style="color: red">${error.defaultMessage}</a><br>
    </#list>
</#if>

<a href="register">Register new user</a>


</body>
</html>