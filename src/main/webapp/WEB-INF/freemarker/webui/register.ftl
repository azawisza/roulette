<html>
<head>
    <title>Register</title>
</head>
<body>

<form action="addUserForm" method="post">
    Username:<br>
    <input type="text" name="username"/><br>
    Password:<br>
    <input type="text" name="password" readonly="true" value="${generatedPassword}" width="200"/><br><br>
    <input type="submit" value="Add New User"/>
</form>
Enter unique username (alpha numeric, min 5 characters)
<br><br>
<#if message??>
<h3>${message}</h3>
</#if>

<#if errors??>
    <#list errors as error>
    <a style="color: red">${error.defaultMessage}</a><br>
    </#list>
</#if>

</body>
</html>