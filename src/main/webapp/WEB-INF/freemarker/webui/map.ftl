<html>
<head>
    <title>This is simple map</title>
    <style>
        #map-canvas {
            width: 500px;
            height: 400px;
            background-color: #CCC;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() {
        }
        var mapCanvas = document.getElementById('map-canvas');
        var map = new google.maps.Map(mapCanvas, {
            center: new google.maps.LatLng(44.5403, -78.5463),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP //ROADMAP, SATELLITE, HYBRID, or TERRAIN.
        });
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>

<a href="register">Register new user</a>


<div id="map-canvas"></div>


</body>
</html>