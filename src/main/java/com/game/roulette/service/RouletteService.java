package com.game.roulette.service;

import com.google.common.base.Optional;
import com.game.roulette.service.model.RouletteStatus;
import com.game.roulette.service.model.UserBetRequest;

public interface RouletteService {

    public RouletteStatus placeBet(UserBetRequest userBetRequest);

    public Optional<String> getCurrentGameEndTime();

}
