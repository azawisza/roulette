package com.game.roulette.service.validation;

import org.springframework.stereotype.Component;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;

@Component
public class StakeValidator {

    public boolean isDouble(String input) {
        try {
            Double parsed = Double.parseDouble(input);
            return !(parsed == POSITIVE_INFINITY || parsed == NEGATIVE_INFINITY);
        } catch (Exception e) {
            return false;
        }
    }

}
