package com.game.roulette.service.validation;

import com.google.common.base.Optional;
import com.game.roulette.service.model.UserBetRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static com.google.common.base.Optional.of;

@Component
public class BetRequestValidator {


    private Validator validator;
    private ConstraintViolationConverter<UserBetRequest> converter;
    private final BetValidator betValidator;
    private final StakeValidator stakeValidator;

    @Autowired
    public BetRequestValidator(BetValidator betValidator, StakeValidator stakeValidator, Validator validator) {
        this.betValidator = betValidator;
        this.stakeValidator = stakeValidator;
        this.validator = validator;
        converter = new ConstraintViolationConverter<UserBetRequest>();
    }

    public Optional<String> validate(UserBetRequest userBetRequest) {
        Set<ConstraintViolation<UserBetRequest>> validate = validator.validate(userBetRequest);
        StringBuilder builder = new StringBuilder();
        if (!validate.isEmpty()) {
            builder.append(converter.convertToMessage(validate) + " ");
        }
        if (!betValidator.isValid(userBetRequest.getBet())) {
            builder.append("Bet is invalid ");
        }
        if (!stakeValidator.isDouble(userBetRequest.getStake())) {
            builder.append("Stake is invalid ");
        }
        return builder.length() > 0 ? of(builder.toString()) : Optional.<String>absent();
    }


}
