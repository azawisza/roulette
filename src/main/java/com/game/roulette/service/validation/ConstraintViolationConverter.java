package com.game.roulette.service.validation;

import javax.validation.ConstraintViolation;
import java.util.Set;


public class ConstraintViolationConverter<T> {

    public String convertToMessage(Set<ConstraintViolation<T>> set) {
        StringBuilder builder = new StringBuilder();
        for (ConstraintViolation<T> violation : set) {
            builder.append(violation.getMessage());
            builder.append(" ");
        }
        return builder.toString();
    }
}
