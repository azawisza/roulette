package com.game.roulette.service.validation;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import static com.game.roulette.commons.GameConsts.MAX_ROULETTE_NUMBER;
import static com.game.roulette.commons.GameConsts.MIN_ROULETTE_NUMBER;
import static com.game.roulette.domain.bet.BetParity.EVEN;
import static com.game.roulette.domain.bet.BetParity.ODD;

@Component
public class BetValidator {

    private static final Set<String> betValues = new HashSet<String>();

    static {
        betValues.add(EVEN.name());
        betValues.add(ODD.name());
        for (int i = MIN_ROULETTE_NUMBER; i <= MAX_ROULETTE_NUMBER; i++) {
            betValues.add(Integer.toString(i));
        }
    }

    public boolean isValid(String bet) {
        return betValues.contains(bet);
    }

}
