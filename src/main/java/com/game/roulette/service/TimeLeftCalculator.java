package com.game.roulette.service;

import com.game.roulette.commons.RouletteConfiguration;
import com.game.roulette.domain.game.Game;
import com.google.common.base.Optional;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class TimeLeftCalculator {

    public static final String TIME_LEFT_FORMAT = "mm:ss";
    private final RouletteConfiguration configuration;
    private final CurrentTimeProvider timeProvider;

    @Autowired
    public TimeLeftCalculator(RouletteConfiguration configuration, CurrentTimeProvider timeProvider) {
        this.configuration = configuration;
        this.timeProvider = timeProvider;
    }

    public String calculateTimeToCurrentGameEnd(Optional<Game> game) {
        long time = game.get().getStartTime().getTime();
        int roundTimeMs = configuration.getRoundTime() * 1000;
        long currentTime = timeProvider.getCurrentTime();

        long expectedEndGame = time + roundTimeMs;
        long timeToEnd = expectedEndGame - currentTime;
        return DateTimeFormat.forPattern(TIME_LEFT_FORMAT).print(timeToEnd);
    }
}
