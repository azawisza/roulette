package com.game.roulette.service.model;

import java.util.List;

public class ResultMessage {

    private List<PlayerBetResult> result;
    private int winningNumber;

    public List<PlayerBetResult> getResult() {
        return result;
    }

    public void setResult(List<PlayerBetResult> result) {
        this.result = result;
    }

    public int getWinningNumber() {
        return winningNumber;
    }

    public void setWinningNumber(int winningNumber) {
        this.winningNumber = winningNumber;
    }

    public ResultMessage withResult(final List<PlayerBetResult> result) {
        this.result = result;
        return this;
    }

    public ResultMessage withWinningNumber(final int winningNumber) {
        this.winningNumber = winningNumber;
        return this;
    }

}
