package com.game.roulette.service.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.game.commons.ToStringSettings.getToStringBuilder;

public class UserBetRequest {

    @NotNull(message = "The username may not be empty.")
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "The username must to be non-empty alphanumeric only")
    @Length.List({
            @Length(min = 5, message = "The username must be at least 5 characters"),
            @Length(max = 32, message = "The username must be less than 32 characters")
    })
    private String username;
    @NotNull(message = "The username may not be empty.")
    private String bet;
    @NotNull(message = "The username may not be empty.")
    private String stake;

    public String getUsername() {
        return username;
    }

    public String getStake() {
        return stake;
    }

    public String getBet() {
        return bet;
    }

    public UserBetRequest withUsername(final String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBet(String bet) {
        this.bet = bet;
    }

    public void setStake(String stake) {
        this.stake = stake;
    }

    public UserBetRequest withStake(final String stake) {
        this.stake = stake;
        return this;
    }

    public UserBetRequest withBet(final String bet) {
        this.bet = bet;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
