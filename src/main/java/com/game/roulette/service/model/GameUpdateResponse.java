package com.game.roulette.service.model;

public class GameUpdateResponse {

    private String update;

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public GameUpdateResponse withUpdate(final String update) {
        this.update = update;
        return this;
    }

}
