package com.game.roulette.service.model;

import com.game.roulette.domain.bet.Outcome;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import static org.apache.commons.lang.builder.ToStringStyle.SIMPLE_STYLE;

public class PlayerBetResult {

    private String player;
    private String bet;
    private Outcome outcome;
    private String winnings;

    public String getPlayer() {
        return player;
    }

    public String getBet() {
        return bet;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public String getWinnings() {
        return winnings;
    }

    public PlayerBetResult withUser(final String user) {
        this.player = user;
        return this;
    }

    public PlayerBetResult withBet(final String bet) {
        this.bet = bet;
        return this;
    }

    public PlayerBetResult withOutcome(final Outcome outcome) {
        this.outcome = outcome;
        return this;
    }

    public PlayerBetResult withWinnings(final String winnings) {
        this.winnings = winnings;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, SIMPLE_STYLE, true, true);
    }


}
