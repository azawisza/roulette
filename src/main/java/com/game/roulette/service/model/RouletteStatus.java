package com.game.roulette.service.model;


public enum RouletteStatus {
    NO_GAME("No game in progress."),
    BET_PLACED("Bet placed."),
    BET_EXISTS("Bet already set."),
    BET_INVALID;

    private String message;

    private RouletteStatus() {
    }

    private RouletteStatus(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public RouletteStatus withMessage(final String message) {
        this.message = message;
        return this;
    }

}
