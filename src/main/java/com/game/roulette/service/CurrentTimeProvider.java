package com.game.roulette.service;

import org.springframework.stereotype.Component;

@Component
public class CurrentTimeProvider {

    public long getCurrentTime() {
        return System.currentTimeMillis();
    }
}
