package com.game.roulette.service;

import com.game.ddd.ApplicationService;
import com.game.roulette.domain.game.Roulette;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


@ApplicationService
public class AdminRouletteService {

    private final Roulette roulette;

    @Autowired
    public AdminRouletteService(Roulette roulette) {
        this.roulette = roulette;
    }

    @PostConstruct //There is no admin UI, so game starts after dependency injection is done.
    public void startGame() {
        roulette.initializeGame();
    }
}
