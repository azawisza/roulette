package com.game.roulette.service;

import com.game.ddd.ApplicationService;
import com.game.roulette.domain.bet.Bet;
import com.game.roulette.domain.bet.BetFactory;
import com.game.roulette.domain.bet.BetAdder;
import com.game.roulette.domain.game.Game;
import com.game.roulette.service.model.RouletteStatus;
import com.game.roulette.service.model.UserBetRequest;
import com.game.roulette.service.validation.BetRequestValidator;
import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.logging.Logger;

import static com.game.roulette.service.model.RouletteStatus.*;

@ApplicationService
public class RouletteServiceImpl implements RouletteService {

    private final BetAdder betAdder;
    private final BetRequestValidator validator;
    private final BetFactory betFactory;
    private final CurrentGameSupplier currentGame;
    private final TimeLeftCalculator timeCalculator;

    @Autowired
    public RouletteServiceImpl(BetAdder betAdder,
                               BetRequestValidator validator,
                               BetFactory betFactory,
                               CurrentGameSupplier currentGame, TimeLeftCalculator timeCalculator) {
        this.betAdder = betAdder;
        this.validator = validator;
        this.betFactory = betFactory;
        this.currentGame = currentGame;
        this.timeCalculator = timeCalculator;
    }

    @Override
    public RouletteStatus placeBet(UserBetRequest userBetRequest) {
        Optional<String> validationResult = validator.validate(userBetRequest);
        if (validationResult.isPresent()) {
            return BET_INVALID.withMessage(validationResult.get());
        }
        Optional<Game> game = this.currentGame.get();
        if (game.isPresent()) {
            return placeBet(userBetRequest, game);
        } else {
            return NO_GAME;
        }
    }

    @Override
    public Optional<String> getCurrentGameEndTime() {
        Optional<Game> game = currentGame.get();
        if (game.isPresent()) {
            return Optional.of(timeCalculator.calculateTimeToCurrentGameEnd(game));
        } else {
            return Optional.absent();
        }
    }

    private RouletteStatus placeBet(UserBetRequest userBetRequest, Optional<Game> game) {
        Bet bet = betFactory.createBet(userBetRequest, game.get());
        if (betAdder.addIfAbsent(bet)) {
            return BET_PLACED;
        } else {
            return BET_EXISTS;
        }
    }




}
