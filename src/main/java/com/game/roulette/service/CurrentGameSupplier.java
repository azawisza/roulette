package com.game.roulette.service;

import com.game.ddd.EventListener;
import com.game.roulette.domain.game.Game;
import com.game.roulette.domain.game.events.CurrentGameChange;
import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;


@EventListener
public class CurrentGameSupplier {

    private Game currentGame;

    @Autowired
    public CurrentGameSupplier(EventBus eventBus) {
        eventBus.register(this);
    }

    public Optional<Game> get() {
        return currentGame != null ? Optional.of(currentGame) : Optional.<Game>absent();
    }

    @Subscribe
    public void update(CurrentGameChange event) {
        this.currentGame = event.getNewGame();
    }

}
