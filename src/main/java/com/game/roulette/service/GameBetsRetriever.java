package com.game.roulette.service;

import com.game.roulette.domain.bet.Bet;
import com.game.roulette.domain.bet.BetReport;
import com.game.roulette.domain.bet.BetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Component
public class GameBetsRetriever {

    private BetsRepository betsRepository;

    @Autowired
    public GameBetsRetriever(BetsRepository betsRepository) {
        this.betsRepository = betsRepository;
    }


    public List<BetReport> getAllCurrent(String gameId) {
        List<Bet> bets = betsRepository.findAllCurrentBets(gameId);
        List<BetReport> result = new ArrayList<BetReport>();
        for (Bet bet : bets) {
            result.add(new BetReport(
                            bet,
                            new BigInteger(bet.getStake()))
            );
        }
        return result;
    }
}
