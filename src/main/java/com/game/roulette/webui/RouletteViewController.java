package com.game.roulette.webui;

import com.game.roulette.service.RouletteService;
import com.game.roulette.service.model.UserBetRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.function.Supplier;

@Controller
public class RouletteViewController {

    private final RouletteService rouletteService;

    @Autowired
    @Qualifier("socketsServerUrl")
    private Supplier<String> serverUrl;

    @Autowired
    public RouletteViewController(RouletteService rouletteService) {
        this.rouletteService = rouletteService;
    }

    @RequestMapping(value = "/application", method = RequestMethod.GET)
    public ModelAndView application() {
        return new ModelAndView("/webui/application")
                .addObject("serverUrl",serverUrl.get());
    }

    @RequestMapping(value = "/placeBet", method = RequestMethod.POST)
    public ModelAndView placeBet(@ModelAttribute @Valid UserBetRequest betRequest) {
        String status = rouletteService.placeBet(betRequest).getMessage();
        return new ModelAndView("/webui/application")
                .addObject("message", status)
                .addObject("serverurl",serverUrl.get())
                .addObject("username", betRequest.getUsername());
    }

}


