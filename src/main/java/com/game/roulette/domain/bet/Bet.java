package com.game.roulette.domain.bet;

import com.game.ddd.AggregateRoot;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import static org.apache.commons.lang.builder.ToStringStyle.SIMPLE_STYLE;

@AggregateRoot
public class Bet {


    @MongoId
    @MongoObjectId
    public String _id;

    private Integer value;
    private String username;
    private BetParity betParity;
    private String stake;
    @MongoObjectId
    private String gameId;

    public Bet() {
    }

    public Bet(String username, Integer value) {
        this.username = username;
        this.value = value;
        betParity = null;
    }

    public Bet(String username, BetParity betParity) {
        this.username = username;
        this.betParity = betParity;
        value = null;
    }

    public String _id() {
        return this._id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public boolean isNumeric() {
        return value != null;
    }

    public BetParity getBetParity() {
        return betParity;
    }

    public Integer getValue() {
        return value;
    }

    public String getStake() {
        return stake;
    }

    public void setStake(String stake) {
        this.stake = stake;
    }

    public Bet withStake(final String stake) {
        this.stake = stake;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, SIMPLE_STYLE, true, true);
    }

    public Bet withGameId(String gameId) {
        this.gameId = gameId;
        return this;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
