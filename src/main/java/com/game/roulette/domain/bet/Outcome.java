package com.game.roulette.domain.bet;

public enum Outcome {
    WIN, LOSE
}
