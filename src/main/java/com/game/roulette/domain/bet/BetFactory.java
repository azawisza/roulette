package com.game.roulette.domain.bet;

import com.game.ddd.DomainFactory;
import com.game.roulette.domain.game.Game;
import com.game.roulette.service.model.UserBetRequest;
import org.springframework.beans.factory.annotation.Autowired;

@DomainFactory
public class BetFactory {

    private final BetRequestToBetConverter betConverter;

    @Autowired
    public BetFactory(BetRequestToBetConverter betConverter) {
        this.betConverter = betConverter;
    }

    public Bet createBet(UserBetRequest request, Game game) {
        Bet bet = betConverter.convert(request);
        return bet.withGameId(game.get_id());
    }

}

