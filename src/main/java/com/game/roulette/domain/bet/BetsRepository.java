package com.game.roulette.domain.bet;

import com.google.common.collect.ImmutableList;
import com.game.ddd.DomainRepository;
import org.bson.types.ObjectId;
import org.jongo.Find;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

@DomainRepository
public class BetsRepository {

    public static final String collection = "bets";
    public static final int BETS_QUERY_LIMIT = 10000;

    @Autowired
    public Jongo jongo;

    public void insert(Bet bet) {
        collection().insert(bet);
    }

    public List<Bet> findAllCurrentBets(String gameId) {
        return getBets(collection().find("{gameId:#}", new ObjectId(gameId)));
    }

    public List<Bet> findBet(Bet bet) {
        return getBets(collection().find("{gameId:#,username:#,betParity:#,value:#},{sort:{username:1, placeDate:1}}",
                new ObjectId(bet.getGameId()), bet.getUsername(), bet.getBetParity(), bet.getValue()));
    }

    private List<Bet> getBets(Find find) {
        MongoCursor<Bet> lessonsCursor = find.limit(BETS_QUERY_LIMIT).as(Bet.class);
        ImmutableList<Bet> bets = ImmutableList.<Bet>builder().addAll(lessonsCursor.iterator()).build();
        closeCursor(lessonsCursor);
        return bets;
    }

    private MongoCollection collection() {
        return jongo.getCollection(collection);
    }

    private void closeCursor(MongoCursor<Bet> lessonsCursor) {
        try {
            lessonsCursor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
