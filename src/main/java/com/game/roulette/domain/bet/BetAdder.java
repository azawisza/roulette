package com.game.roulette.domain.bet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class BetAdder {

    private BetsRepository betsRepository;

    @Autowired
    public BetAdder(BetsRepository betsRepository) {
        this.betsRepository = betsRepository;
    }

    public boolean addIfAbsent(Bet bet) {
        if (betsRepository.findBet(bet).isEmpty()) {
            betsRepository.insert(bet);
            return true;
        } else {
            return false;
        }
    }

}
