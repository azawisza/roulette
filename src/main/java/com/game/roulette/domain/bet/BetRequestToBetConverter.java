package com.game.roulette.domain.bet;

import com.game.roulette.service.model.UserBetRequest;
import org.springframework.stereotype.Component;

import static com.game.roulette.domain.bet.BetParity.getBetParityFor;
import static com.game.roulette.domain.bet.BetParity.isParityBet;

@Component
public class BetRequestToBetConverter {

    public Bet convert(UserBetRequest request) {
        if (isParityBet(request.getBet())) {
            return new Bet(request.getUsername(), getBetParityFor(request.getBet()))
                    .withStake(request.getStake());
        } else {
            return new Bet(request.getUsername(), new Integer(request.getBet()))
                    .withStake(request.getStake());
        }
    }

}
