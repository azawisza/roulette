package com.game.roulette.domain.bet;


public enum BetParity {

    EVEN, ODD, NONE;

    public static BetParity getBetParityFor(String value) {
        for (BetParity betParity : values()) {
            if (betParity.name().equals(value)) {
                return betParity;
            }
        }
        throw new IllegalArgumentException("no value for: " + value);
    }

    public static boolean isParityBet(String bet) {
        for (BetParity betParity : values()) {
            if (betParity.name().equals(bet)) {
                return true;
            }
        }
        return false;
    }

    public static BetParity getBetParityFor(int number) {
        if (number % 2 == 0) {
            return EVEN;
        } else {
            return ODD;
        }
    }

}
