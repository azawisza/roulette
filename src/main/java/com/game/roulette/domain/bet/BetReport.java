package com.game.roulette.domain.bet;

import java.math.BigInteger;

import static com.game.roulette.domain.bet.BetParity.getBetParityFor;
import static com.game.roulette.domain.bet.Outcome.LOSE;
import static com.game.roulette.domain.bet.Outcome.WIN;

public class BetReport implements Comparable<BetReport> {

    private Bet bet;
    private BigInteger stake;

    public BetReport(Bet bet, BigInteger stake) {
        this.bet = bet;
        this.stake = stake;
    }

    public boolean isNumeric() {
        return bet.isNumeric();
    }

    public String getUsername() {
        return bet.getUsername();
    }

    public String getBet() {
        if (bet.isNumeric()) {
            return bet.getValue().toString();
        } else {
            return bet.getBetParity().name();
        }
    }

    public BigInteger getStake() {
        return stake;
    }

    public boolean isWinning(int winningNumber) {
        if (bet.isNumeric()) {
            return bet.getValue().equals(winningNumber);
        } else {
            return bet.getBetParity().equals(getBetParityFor(winningNumber));
        }
    }

    public Outcome getOutcome(int winningNumber) {
        if (isWinning(winningNumber)) {
            return WIN;
        } else {
            return LOSE;
        }
    }

    @Override
    public int compareTo(BetReport other) {
        return getUsername().compareTo(other.getUsername());
    }

}
