package com.game.roulette.domain.game;


import com.game.ddd.DomainRepository;
import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@DomainRepository
public class GamesRepository {

    public static final String collection = "games";

    @Autowired
    public Jongo jongo;

    public void insert(Game game) {
        jongo.getCollection(collection).insert(game);
    }

    public Game findOne(Date gameDate) {
        return jongo.getCollection(collection).findOne("{startTime: #}", gameDate).as(Game.class);
    }

    public void update(String currentGameId, int winningNumber, Date endTime) {
        try {
            jongo.getCollection(collection).update(new ObjectId(currentGameId))
                    .with("{$set:{endTime:#,winningNumber:#}}", endTime, winningNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
