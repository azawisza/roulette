package com.game.roulette.domain.game;

import com.game.roulette.domain.bet.BetReport;
import com.game.roulette.service.model.PlayerBetResult;

import java.math.BigInteger;


public class BetCalculator {

    public static final int NUMERIC_BET_STAKE_MULTIPLIER = 36;
    public static final int PARITY_BET_STAKE_MULTIPLIER = 2;
    private final int winning;

    public BetCalculator(int winningNumber) {
        this.winning = winningNumber;
    }

    public PlayerBetResult calculate(BetReport report) {
        return new PlayerBetResult()
                .withUser(report.getUsername())
                .withBet(report.getBet())
                .withOutcome(report.getOutcome(winning))
                .withWinnings(calculateWinnings(report).toString());
    }

    private BigInteger calculateWinnings(BetReport report) {
        if (report.isWinning(winning)) {
            if (report.isNumeric()) {
                return multiplyBy(report.getStake(), NUMERIC_BET_STAKE_MULTIPLIER);
            } else {
                return multiplyBy(report.getStake(), PARITY_BET_STAKE_MULTIPLIER);
            }
        } else {
            return new BigInteger("0");
        }
    }

    private BigInteger multiplyBy(BigInteger bigInteger, int multiplier) {
        return bigInteger.multiply(new BigInteger(Integer.toString(multiplier)));
    }

}
