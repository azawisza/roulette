package com.game.roulette.domain.game.events;

import com.game.ddd.Event;
import com.game.roulette.service.model.ResultMessage;

@Event
public class PublishLastGameResults {

    private ResultMessage resultMessage;

    public PublishLastGameResults(ResultMessage resultMessage) {
        this.resultMessage = resultMessage;
    }

    public ResultMessage getResultMessage() {
        return resultMessage;
    }

}
