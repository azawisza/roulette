package com.game.roulette.domain.game.events;

import com.game.ddd.Event;
import com.game.roulette.domain.game.Game;

@Event
public class CurrentGameChange {

    private final Game newGame;

    public static CurrentGameChange stopGameEvent(){
        return new CurrentGameChange(null);
    }
    public CurrentGameChange(Game game) {
        this.newGame = game;
    }

    public Game getNewGame() {
        return newGame;
    }

}
