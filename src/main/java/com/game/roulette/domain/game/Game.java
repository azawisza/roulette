package com.game.roulette.domain.game;

import com.game.ddd.AggregateRoot;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.Date;

@AggregateRoot
public class Game {

    @MongoId
    @MongoObjectId
    private String _id;

    private int winningNumber;
    private Date startTime;
    private Date endTime;

    public Game() {
    }

    public Game(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getWinningNumber() {
        return winningNumber;
    }

    public void setWinningNumber(int winningNumber) {
        this.winningNumber = winningNumber;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Game with_id(final String _id) {
        this._id = _id;
        return this;
    }

    public Game withWinningNumber(final int winningNumber) {
        this.winningNumber = winningNumber;
        return this;
    }

    public Game withStartTime(final Date startTime) {
        this.startTime = startTime;
        return this;
    }

    public Game withEndTime(final Date endTime) {
        this.endTime = endTime;
        return this;
    }


}
