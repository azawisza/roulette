package com.game.roulette.domain.game;

import com.game.roulette.domain.bet.BetReport;
import com.game.roulette.service.GameBetsRetriever;
import com.game.roulette.domain.game.events.CurrentGameChange;
import com.game.roulette.domain.game.events.PublishLastGameResults;
import com.game.roulette.service.model.PlayerBetResult;
import com.game.roulette.service.model.ResultMessage;
import com.google.common.base.Function;
import com.google.common.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static com.game.roulette.domain.game.events.CurrentGameChange.stopGameEvent;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.sort;

@Component
public class GameRound implements Runnable {

    private final GameBetsRetriever betsRetriever;
    private final WinningRouletteNumberSupplier winningNumberSupplier;
    private final EventBus eventBus;
    private final GamesRepository gamesRepository;
    static final Logger logger = Logger.getLogger(GameRound.class.getName());

    private String currentGameId;

    @Autowired
    public GameRound(GameBetsRetriever betsRetriever,
                     WinningRouletteNumberSupplier winningNumberSupplier,
                     EventBus eventBus,
                     GamesRepository gamesRepository) {
        this.betsRetriever = betsRetriever;
        this.winningNumberSupplier = winningNumberSupplier;
        this.eventBus = eventBus;
        this.gamesRepository = gamesRepository;
        this.currentGameId = setUpGameRound();
    }

    private String setUpGameRound() {
        Date startTime = new Date();
        gamesRepository.insert(new Game(startTime));
        Game game = gamesRepository.findOne(startTime);
        eventBus.post(new CurrentGameChange(game));
        return game.get_id();
    }

    @Override
    public void run() {
        if (currentGameId == null) {
            currentGameId = setUpGameRound();
        }
        eventBus.post(stopGameEvent());
        logger.info("game stopped");
        List<BetReport> reports = betsRetriever.getAllCurrent(currentGameId);
        logger.info("retrieving players");
        int winningNumber = winningNumberSupplier.getTheWinningNumber();
        gamesRepository.update(currentGameId, winningNumber, new Date());
        logger.info("game updated ");
        List<PlayerBetResult> results = evaluateBets(reports, winningNumber);

        eventBus.post(new PublishLastGameResults(new ResultMessage()
                .withResult(results)
                .withWinningNumber(winningNumber)));
        logger.info("results sent ");
        currentGameId = setUpGameRound();
        logger.info("new game set");
    }

    private List<PlayerBetResult> evaluateBets(List<BetReport> reports, int winningNumber) {
        final BetCalculator calculator = new BetCalculator(winningNumber);
        sort(reports);
        return newArrayList(transform(reports, new Function<BetReport, PlayerBetResult>() {
            @Override
            public PlayerBetResult apply(BetReport report) {
                return calculator.calculate(report);
            }
        }));
    }
}
