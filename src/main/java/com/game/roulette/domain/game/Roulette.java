package com.game.roulette.domain.game;

import com.game.ddd.DomainService;
import com.game.roulette.commons.RouletteConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PreDestroy;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;

@DomainService
public class Roulette {

    private final ScheduledExecutorService gameScheduler;
    private final RouletteConfiguration configuration;
    private final GameRound gameRound;

    @Autowired
    public Roulette(RouletteConfiguration configuration,
                    GameRound gameRound) {
        this.configuration = configuration;
        gameScheduler = newSingleThreadScheduledExecutor();
        this.gameRound = gameRound;
    }

    public void initializeGame() {
        gameScheduler.scheduleAtFixedRate(gameRound, configuration.getInitialGameDelay(),
                configuration.getRoundTime(), TimeUnit.SECONDS);
    }

    @PreDestroy
    public void shutdown() {
        gameScheduler.shutdown();
    }
}
