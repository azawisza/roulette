package com.game.roulette.domain.game;

import org.springframework.stereotype.Component;

import java.util.Random;

import static com.game.roulette.commons.GameConsts.MAX_ROULETTE_NUMBER;
import static com.game.roulette.commons.GameConsts.MIN_ROULETTE_NUMBER;

@Component
public class WinningRouletteNumberSupplier {

    public int getTheWinningNumber() {
        return getRandomIntegerWithinRangeOf(MIN_ROULETTE_NUMBER, MAX_ROULETTE_NUMBER);
    }

    private int getRandomIntegerWithinRangeOf(int min, int max) {
        Random r = new Random();
        max++;
        return r.nextInt(max - min) + min;
    }

}
