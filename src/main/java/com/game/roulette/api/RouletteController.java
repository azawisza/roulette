package com.game.roulette.api;

import com.game.ddd.EventListener;
import com.game.roulette.domain.game.GameRound;
import com.game.roulette.domain.game.events.PublishLastGameResults;
import com.game.roulette.service.RouletteService;
import com.game.roulette.service.model.GameUpdateResponse;
import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.logging.Logger;

import static com.game.roulette.commons.MessagingConsts.PUBLISH_RESULTS_TOPIC;
import static com.game.roulette.commons.MessagingConsts.PUBLISH_TIME_TOPIC;

@Controller
@EventListener
public class RouletteController {
    private final RouletteService service;
    private final SimpMessageSendingOperations messaging;
    private static final Logger logger = Logger.getLogger(GameRound.class.getName());

    @Autowired
    public RouletteController(RouletteService service, SimpMessageSendingOperations messaging, EventBus eventBus) {
        this.service = service;
        this.messaging = messaging;
        eventBus.register(this);
    }

    @Scheduled(fixedRate = 1000)
    public void publishNextRoundTime() {
        Optional<String> endGame = service.getCurrentGameEndTime();

        if (endGame.isPresent()) {
            String currentGameEndTime = endGame.get();
            messaging.convertAndSend(PUBLISH_TIME_TOPIC, new GameUpdateResponse().withUpdate(currentGameEndTime));
        } else {
            logger.info("No game in progress");
            messaging.convertAndSend(PUBLISH_TIME_TOPIC, new GameUpdateResponse().withUpdate("Please wait for the nex round"));
        }
    }

    @Subscribe
    public void publishNextRoundTime(PublishLastGameResults results) {
        messaging.convertAndSend(PUBLISH_RESULTS_TOPIC, results.getResultMessage());
    }


}
