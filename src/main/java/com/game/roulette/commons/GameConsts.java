package com.game.roulette.commons;

public class GameConsts {
    public static final int MAX_USER_NAME_LENGTH = 64;
    public static final int MAX_ROULETTE_NUMBER = 36;
    public static final int MIN_ROULETTE_NUMBER = 1;
}
