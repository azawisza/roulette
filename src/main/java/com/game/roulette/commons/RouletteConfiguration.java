package com.game.roulette.commons;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RouletteConfiguration {

    @Value("${game.roundTime}")
    private int roundTime;

    @Value("${game.initialGameDelay}")
    private int initialGameDelay;

    public int getRoundTime() {
        return roundTime;
    }

    public int getInitialGameDelay() {
        return initialGameDelay;
    }
}
