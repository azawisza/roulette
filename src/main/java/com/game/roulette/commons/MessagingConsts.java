package com.game.roulette.commons;

public class MessagingConsts {

    public static final String PUBLISH_RESULTS = "publishResults";
    public static final String PUBLISH_TIME = "publishTime";
    public static final String BROKER_NAME = "topic";
    public static final String PUBLISH_RESULTS_TOPIC = "/" + BROKER_NAME + "/" + PUBLISH_RESULTS;
    public static final String PUBLISH_TIME_TOPIC = "/" + BROKER_NAME + "/" + PUBLISH_TIME;
    public static final String APPLICATION_DESTINATION = "app";
    public static final String ENDPOINT_GAME = "game";


}
