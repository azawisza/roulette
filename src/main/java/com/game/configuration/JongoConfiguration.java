package com.game.configuration;

import com.github.fakemongo.Fongo;
import com.mongodb.DB;
import org.jongo.Jongo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.net.UnknownHostException;

@Configuration
public class JongoConfiguration {

    @Value("${mongodb.database}")
    public String database;

    @Value("${mongodb.username}")
    public String username;

    @Value("${mongodb.password}")
    public String password;

    @Value("${mongodb.host}")
    public String host;

    @Value("${mongodb.port}")
    public String port;

    @Value("${spring.profile}")
    public String profile;

//Uncomment for real database:
/*    @Bean
    public Jongo createJongo() throws UnknownHostException {
       if(profile.equals(SPRING_PROFILE_DEVELOPMENT)){
           DB db = new MongoClient().getDB(database);
           return new Jongo(db);
       }else {
           MongoCredential credential = createCredential(username, database, password.toCharArray());
           MongoClient mongoClient = new MongoClient(new ServerAddress(host, parseInt(port)), asList(credential));
           DB db = mongoClient.getDB(database);
           return new Jongo(db);
       }
    }*/


//Mock database
    @Bean
    public Jongo createJongo() throws UnknownHostException {
        DB db = new Fongo("Test").getDB(database);
        Jongo jongo = new Jongo(db);
        return jongo;
    }

}
