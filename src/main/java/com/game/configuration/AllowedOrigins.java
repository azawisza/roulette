package com.game.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "websockets")
public class AllowedOrigins {

    private List<String> allowedorigins;

    public List<String> getAllowedorigins() {
        return allowedorigins;
    }

    public void setAllowedorigins(List<String> allowedorigins) {
        this.allowedorigins = allowedorigins;
    }
}
