package com.game.configuration;

public class ApplicationProfileConsts {

    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_TEST = "test";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";
    public static final String PROFILE_PROPERTY_NAME = "spring.profiles.active";//-Dspring.profiles.active=dev

}
