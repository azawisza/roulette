package com.game.configuration;

import com.google.common.eventbus.EventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GuavaEventBusConfiguration {

    @Bean
    public EventBus eventBus() {
        return new EventBus();
    }
}
