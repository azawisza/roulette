package com.game.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Configuration
public class FreemarkerConfiguration {

    @Autowired
    public FreeMarkerViewResolver freeMarkerViewResolver;

    public String EXTENSION = ".ftl";

    @Bean
    public FreeMarkerConfigurer getFreeMarkerConfigurer() throws IOException {
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPath("classpath:WEB-INF/freemarker");
        return freeMarkerConfigurer;
    }

    @PostConstruct
    private void init() {
        freeMarkerViewResolver.setPrefix("");
        freeMarkerViewResolver.setSuffix(EXTENSION);
        freeMarkerViewResolver.setCache(false);
        freeMarkerViewResolver.setExposeSpringMacroHelpers(true);
    }

}

