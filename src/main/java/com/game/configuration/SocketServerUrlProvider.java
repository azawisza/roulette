package com.game.configuration;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Supplier;

/**
 * Created by azawisza on 12.11.2016.
 */
@Configuration
public class SocketServerUrlProvider {

    @Value("${websockets.serverUrl}")
    private String serverUrl;

    @Value("${websockets.serverSuffix}")
    private String suffix;

    @Value("${server.port}")
    private String port;

    @Bean("socketsServerUrl")
    public Supplier<String> create() {
        return () -> serverUrl + ":" + port + "/" + suffix;
    }

}
