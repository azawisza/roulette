package com.game.configuration;

import com.game.commons.ErrorResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(annotations = {Controller.class})
public class ViewControllerErrorConfiguration {

    public static final String INTERNAL_ERRROR = "50010001";

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception e) {
        ErrorResponse errorResponse = new ErrorResponse()
                .withCode(INTERNAL_ERRROR)
                .withDetails(e.toString())
                .withMessage("Internal error " + e.getMessage());
        return new ModelAndView("applicationError").addObject("error", errorResponse);

    }

}
