package com.game.configuration;

import com.game.commons.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice(annotations = {RestController.class})
public class RestControllerErrorConfiguration {

    public static final String INTERNAL_ERRROR = "50010001";

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private ErrorResponse handleException(Exception e) {
        return new ErrorResponse()
                .withCode(INTERNAL_ERRROR)
                .withDetails(e.toString())
                .withMessage("Internal error");
    }

}
