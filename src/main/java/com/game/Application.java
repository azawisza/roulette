package com.game;

import org.springframework.boot.SpringApplication;

import java.util.logging.Logger;

import static com.game.configuration.ApplicationProfileConsts.PROFILE_PROPERTY_NAME;
import static com.game.configuration.ApplicationProfileConsts.SPRING_PROFILE_DEVELOPMENT;
import static java.lang.System.getProperty;
import static java.lang.System.setProperty;

public class Application {

    static final Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WebMvcConfiguration.class);
        logger.info("Starting application");
        String profile = getProperty(PROFILE_PROPERTY_NAME);
        if (profile != null) {
            setProperty(PROFILE_PROPERTY_NAME, profile);
        } else {
            profile = SPRING_PROFILE_DEVELOPMENT;
            setProperty(PROFILE_PROPERTY_NAME, SPRING_PROFILE_DEVELOPMENT);
        }
        logger.info(" Selected profile ----> " + profile);
        application.run(args);
    }
}

	

