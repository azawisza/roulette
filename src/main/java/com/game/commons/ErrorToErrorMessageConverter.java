package com.game.commons;

import com.google.common.base.Function;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.List;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

@Component
public class ErrorToErrorMessageConverter {

    public List<String> convert(Errors errors) {
        return newArrayList(transform(errors.getAllErrors(), new Function<ObjectError, String>() {
            @Override
            public String apply(ObjectError input) {
                return input.getDefaultMessage();
            }
        }));
    }

}