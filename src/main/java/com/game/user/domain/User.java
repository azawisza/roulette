package com.game.user.domain;

import com.game.ddd.AggregateRoot;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import static com.game.commons.ToStringSettings.getToStringBuilder;

@AggregateRoot
public class User {

    @MongoId
    @MongoObjectId
    public String userId;

    private String username;
    private String password;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User withPassword(final String password) {
        this.password = password;
        return this;
    }

    public User withUsername(final String username) {
        this.username = username;
        return this;
    }

    public User withUserId(final String userId) {
        this.userId = userId;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


}
