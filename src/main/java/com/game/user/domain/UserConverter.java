package com.game.user.domain;

import com.game.user.service.model.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public User convert(UserDto user) {
        return new User().withPassword(user.getPassword()).withUsername(user.getUsername());
    }

    public UserDto convert(User user) {
        return new UserDto().withPassword(user.getPassword()).withUsername(user.getUsername());
    }
}
