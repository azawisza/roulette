package com.game.user.domain;

import com.game.ddd.DomainRepository;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@DomainRepository
public class UsersRepository {

    @Autowired
    public Jongo jongo;

    public static final String collection = "users";

    public User findUser(String username, String password) {
        return collection().findOne("{username:#,password:#}", username, password).as(User.class);
    }

    public User findUser(String username) {
        return collection().findOne("{username:#}", username).as(User.class);
    }

    public void addUser(User user) {
        collection().insert(user);
    }

    private MongoCollection collection() {
        return jongo.getCollection(collection);
    }

}
