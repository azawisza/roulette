package com.game.user.service.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.game.commons.ToStringSettings.getToStringBuilder;

public class UserDto {

    @NotNull(message = "The username may not be empty.")
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "The username must to be non-empty alphanumeric only")
    @Length.List({
            @Length(min = 5, message = "The username must be at least 5 characters"),
            @Length(max = 32, message = "The username must be less than 32 characters")
    })
    private String username;

    @NotNull(message = "The password may not be empty.")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$", message = "The password must contain 1 number 1 upper case letter 1 lower case letter")
    @Length.List({
            @Length(min = 8, message = "The password must be at least 8 characters")
    })
    private String password;

    public UserDto() {
    }

    public UserDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserDto withUsername(final String username) {
        this.username = username;
        return this;
    }

    public UserDto withPassword(final String password) {
        this.password = password;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
