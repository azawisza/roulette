package com.game.user.service.model;

import java.util.ArrayList;
import java.util.List;

public class AddUserResponse {

    private List<String> errors = new ArrayList<String>();
    private RegistrationResultStatus status;

    public AddUserResponse withErrors(final List<String> errors) {
        this.errors = errors;
        return this;
    }

    public AddUserResponse withStatus(final RegistrationResultStatus status) {
        this.status = status;
        return this;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public RegistrationResultStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationResultStatus status) {
        this.status = status;
    }
}
