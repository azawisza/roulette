package com.game.user.service.model;

public enum RegistrationResultStatus {
    ADDED,
    INVALID,
    EXISTS_ALREADY
}
