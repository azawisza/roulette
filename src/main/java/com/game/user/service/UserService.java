package com.game.user.service;

import com.game.ddd.ApplicationService;
import com.game.user.domain.User;
import com.game.user.domain.UserConverter;
import com.game.user.domain.UsersRepository;
import com.game.user.service.model.RegistrationResultStatus;
import com.game.user.service.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.security.SecureRandom;

import static com.game.user.service.model.RegistrationResultStatus.ADDED;
import static com.game.user.service.model.RegistrationResultStatus.EXISTS_ALREADY;

@ApplicationService
public class UserService implements UsersService {


    public static final String ENSURE_VALID = "A1";
    private final UsersRepository usersRepository;
    private final SecureRandom random = new SecureRandom();
    private final UserConverter userConverter;

    @Autowired
    public UserService(UsersRepository usersRepository, UserConverter userConverter) {
        this.usersRepository = usersRepository;
        this.userConverter = userConverter;
    }

    @Override
    public boolean authenticate(UserDto user) {
        return usersRepository.findUser(user.getUsername(),user.getPassword()) != null;
    }

    @Override
    public RegistrationResultStatus addUserIfAbsent(UserDto user) {
        if (userExists(user)) {
            return EXISTS_ALREADY;
        } else {
            User userEntity = userConverter.convert(user);
            usersRepository.addUser(userEntity);
            return ADDED;
        }
    }

    @Override
    public String generatePassword() {
        String randomString = new BigInteger(130, random).toString(16);
        return randomString.substring(0, 9) + ENSURE_VALID;
    }

    public boolean userExists(UserDto user) {
        return usersRepository.findUser(user.getUsername()) != null;
    }

}
