package com.game.user.service;

import com.game.user.service.model.RegistrationResultStatus;
import com.game.user.service.model.UserDto;

public interface UsersService {

    public boolean authenticate(UserDto user);

    public RegistrationResultStatus addUserIfAbsent(UserDto user);

    public String generatePassword();
}
