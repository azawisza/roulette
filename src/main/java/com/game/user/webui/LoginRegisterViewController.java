package com.game.user.webui;

import com.game.user.service.UsersService;
import com.game.user.service.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.function.Supplier;

import static com.game.user.service.model.RegistrationResultStatus.EXISTS_ALREADY;

@Controller
public class LoginRegisterViewController {

    private final UsersService userService;

    @Autowired
    @Qualifier("socketsServerUrl")
    private Supplier<String> serverUrl;

    @Autowired
    public LoginRegisterViewController(UsersService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        return loginPageWith("Enter credentials.");
    }

    @RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
    public ModelAndView loginCheck(@ModelAttribute @Valid UserDto user, BindingResult result, Errors errors) {

        if (errors.hasErrors()) {
            return loginPageWith("Invalid input.");
        }
        if (userService.authenticate(user)) {
            return startApplication(user);
        } else {
            return loginPageWith("Cannot login. Please verify username or password.");
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView register() {
        return registerPage();
    }

    @RequestMapping(value = "/addUserForm", method = RequestMethod.POST)
    public ModelAndView addUserForm(@ModelAttribute @Valid UserDto user, Errors errors) {
        if (errors.hasErrors()) {
            return registerPage("Invalid input.", errors.getAllErrors());
        }
        if (userService.addUserIfAbsent(user) != EXISTS_ALREADY) {
            return loginPageWith("New user added. You can now log in.");
        } else {
            return registerPage("User already exists.");
        }
    }

    private ModelAndView startApplication(UserDto user) {
        return new ModelAndView("webui/application")
                .addObject("username", user.getUsername())
                .addObject("serverurl",serverUrl.get());
    }

    private ModelAndView registerPage(String attributeValue) {
        ModelAndView register = new ModelAndView("webui/register");
        register.addObject("message", attributeValue).addObject("generatedPassword", userService.generatePassword());
        return register;
    }

    private ModelAndView registerPage(String attributeValue, List<ObjectError> errors) {
        return new ModelAndView("webui/register")
                .addObject("message", attributeValue)
                .addObject("errors", errors)
                .addObject("generatedPassword", userService.generatePassword());
    }

    private ModelAndView registerPage() {
        return registerPage("").addObject("generatedPassword", userService.generatePassword());
    }

    private ModelAndView loginPageWith(String message) {
        return new ModelAndView("webui/login").addObject("message", message);
    }


}
